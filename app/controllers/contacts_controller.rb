class ContactsController < ApplicationController

  def new
    @contact = Contact.new
  end

=begin
  def create
    @contact = Contact.new(secure_params)
    if @contact.valid?
      @contact.update_spreadsheet
      UserMailer.contact_email(@contact).deliver
      flash[:notice] = "Message sent from #{@contact.name}."
      redirect_to root_path
    else
      render :new
    end
  end

=end

  def create
    @contact = Contact.new(secure_params)
    if @contact.valid?
      @contact.subscribe
      flash[:notice] = "Signed up #{@contact.email}."
      redirect_to root_path
    else
      render :new
    end
  end

  private

#  def secure_params
#    params.require(:contact).permit(:name, :email, :content)
#  end

  def secure_params
    params.require(:contact).permit(:email)
  end

end
